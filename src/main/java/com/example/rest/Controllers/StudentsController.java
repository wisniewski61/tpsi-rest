//source: http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/griz_jersey_intro/Grizzly-Jersey-Intro.html
package com.example.rest.Controllers;

import com.example.rest.Models.Course;
import com.example.rest.Models.DataContext;
import com.example.rest.Models.Grade;
import com.example.rest.Models.Student;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("students")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class StudentsController {
    private DataContext dataContext = new DataContext();

    @GET
    public List<Student> getStudents(
            @QueryParam("firstname") String firstName,
            @QueryParam("lastname") String lastName,
            @QueryParam("birthDate") String birthDate,
            @QueryParam("before") Boolean before) {
        List<Student> students = dataContext.getStudents(firstName, lastName, birthDate, before);
        return students;
    }

    @GET
    @Path("{index}")
    public Student getStudent(@PathParam("index") int index) {
        Student student = dataContext.getStudentByIndex(index);
        if (student != null)
            return student;

        throw new NotFoundException();
    }//getStudent()

    @POST
    public Response postStudent(Student student, @Context UriInfo uriInfo) {
        int index = dataContext.addStudent(student, false);
        // source: http://stackoverflow.com/questions/26092318/create-response-with-location-header-in-jax-rs
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Integer.toString(index));
        return Response.created(builder.build()).build();
    }//postStudent()

    @PUT
    @Path("{index}")
    public Response putStudent(@PathParam("index") int index, Student student) {
        if (index != student.getIndex())
            student.setIndex(index);
        //  return Response.notAcceptable();

        if (dataContext.updateStudent(student))
            return Response.ok().build();

        throw new NotFoundException();
    }//putStudent()

    @DELETE
    @Path("{index}")
    public Response deleteStudent(@PathParam("index") int index) {
        Student student = dataContext.getStudentByIndex(index);
        if (student == null)
            throw new NotFoundException();
        if (dataContext.deleteStudent(student))
            return Response.ok().build();
        return Response.serverError().build();
    }//deleteStudent()

    @GET
    @Path("{index}/grades")
    public List<Grade> getGrades(
            @PathParam("index") int index,
            @QueryParam("coursename") String courseName,
            @QueryParam("value") Float value,
            @DefaultValue("false") @QueryParam("higher") boolean higher) {
        List<Grade> grades = dataContext.getStudentGrades(index);
        if (courseName != null)
            grades = filterGradesByCourseName(grades, courseName);
        if (value != null)
            grades = filterGradesByValue(grades, value, higher);
        return grades;
    }//getGrades()

    private List<Grade> filterGradesByValue(List<Grade> grades, Float value, boolean higher) {
        List<Grade> matchingGrades = new ArrayList<>();
        for (Grade g :
                grades) {
            if (g.getValue() == value)
                continue;
            if (g.getValue() > value && higher)
                matchingGrades.add(g);
            if (g.getValue() < value && !higher)
                matchingGrades.add(g);
        }//for
        return matchingGrades;
    }

    private List<Grade> filterGradesByCourseName(List<Grade> grades, String courseName) {
        List<Grade> matchingGrades = new ArrayList<>();
        for (Grade g :
                grades) {
            if (dataContext.getCourseById(g.getCourseId()).getName().contains(courseName))
                matchingGrades.add(g);
        }//for
        return matchingGrades;
    }//filterGradesByCourseName()

    @GET
    @Path("{index}/grades/{id}")
    public Grade getGrade(@PathParam("index") int index, @PathParam("id") int id) {
        Student student = dataContext.getStudentByIndex(index);
        if (student != null) {
            for (Grade grade :
                    student.getGrades()) {
                if (grade.getId() == id)
                    return grade;
            }//for
        }//if

        throw new NotFoundException();
    }//getGrade()

    @POST
    @Path("{index}/grades")
    public Response postGrade(@PathParam("index") int index, Grade grade, @Context UriInfo uriInfo) {
        Course c = grade.getCourse();
        int id = dataContext.addGrade(new Grade(grade.getValue(), grade.getIssueDate(), c.getCourseId()), index);
        // source: http://stackoverflow.com/questions/26092318/create-response-with-location-header-in-jax-rs
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Integer.toString(id));
        return Response.created(builder.build()).build();
    }//postStudent()

    @PUT
    @Path("{index}/grades/{id}")
    public Response putGrade(@PathParam("index") int index, @PathParam("id") int id, Grade grade) {
        //Grade toUpdate = dataContext.getGradeById(id);
        if(grade.getId()!=id)
            grade.setId(id);
        if (dataContext.updateGrade(grade))
            return Response.ok().build();

        throw new NotFoundException();
    }//putStudent()

    @DELETE
    @Path("{index}/grades/{id}")
    public Response deleteGrade(@PathParam("index") int index, @PathParam("id") int id) {
        Student student = dataContext.getStudentByIndex(index);
        Grade grade = dataContext.getGradeById(id);
        if (student == null || grade == null)
            throw new NotFoundException();
        if (dataContext.deleteGrade(grade))
            return Response.ok().build();
        return Response.serverError().build();
    }//deleteStudent()
}
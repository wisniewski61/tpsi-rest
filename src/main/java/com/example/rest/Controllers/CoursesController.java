package com.example.rest.Controllers;

import com.example.rest.Models.Course;
import com.example.rest.Models.DataContext;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 28.04.17.
 */
@Path("courses")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class CoursesController {
    private DataContext dataContext = new DataContext();

    @GET
    public List<Course> getCourses(@QueryParam("lecturer") String lecturer) {
        List<Course> courses = dataContext.getCourses(lecturer);
        return courses;
    }

    @GET
    @Path("{id}")
    public Course getSCourse(@PathParam("id") int id) {
        Course course = dataContext.getCourseById(id);
        if(course!=null)
            return course;

        throw new NotFoundException();
    }//getStudent()

    @POST
    public Response postCourse(Course course, @Context UriInfo uriInfo) {
        int index = dataContext.addCourse(course, false);
        // source: http://stackoverflow.com/questions/26092318/create-response-with-location-header-in-jax-rs
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Integer.toString(index));
        return Response.created(builder.build()).build();
    }//postStudent()

    @PUT
    @Path("{id}")
    public Response putCourse(@PathParam("id") int id, Course course) {
        if(id!=course.getCourseId())
            course.setCourseId(id);
        //  return Response.notAcceptable();

        if(dataContext.updateCourse(course))
            return Response.ok().build();

        throw new NotFoundException();
    }//putStudent()

    @DELETE
    @Path("{id}")
    public Response deleteStudent(@PathParam("id") int id) {
        Course course = dataContext.getCourseById(id);
        if(course==null)
            throw new NotFoundException();
        if(dataContext.deleteCourse(course))
            return Response.ok().build();
        return Response.serverError().build();
    }
}

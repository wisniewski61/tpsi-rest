package com.example.rest.Models;

import com.example.rest.Controllers.StudentsController;
import com.example.rest.Models.Course;
import com.fasterxml.jackson.annotation.JsonFormat;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.glassfish.jersey.linking.Binding;
import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 16.04.17.
 */
@XmlRootElement
public class Grade {
    @InjectLinks({
            @InjectLink(value="students/{index}/grades", bindings = @Binding(name = "index", value = "${instance.getStudentId()}"), rel = "parent"),
            @InjectLink(value="students/{index}/grades/{id}", bindings = @Binding(name = "index", value = "${instance.getStudentId()}"), rel = "self"),
            @InjectLink(value="students/{index}", bindings = @Binding(name = "index", value = "${instance.getStudentId()}"), rel = "student")
            //@InjectLink(resource = StudentsController.class, method = "getStudent", style = InjectLink.Style.ABSOLUTE,
              //      bindings = @Binding(name = "index", value = "${instance.student.index}"), rel = "student")
    })
    @XmlElement(name="link")
    @XmlElementWrapper(name = "links")
    @XmlJavaTypeAdapter(Link.JaxbAdapter.class)
    List<Link> links;

    @XmlTransient
    private int Id;
    private float Value;

    @JsonFormat(shape= JsonFormat.Shape.STRING,
            pattern="yyyy-MM-dd", timezone="CET")
    private Date IssueDate;
    @XmlTransient
    private int CourseId;
    @XmlTransient
    private int StudentId;

    @Reference
    private Course course;

    public Grade() {
        IssueDate = new Date();
    }

    public Grade(float value, Date issueDate, int courseId) {
        Value = value;
        if(issueDate==null)
            issueDate = new Date();
        IssueDate = issueDate;
        CourseId = courseId;
        course = new DataContext().getCourseById(courseId);
        StudentId = 0;
    }

    public float getValue() {
        return Value;
    }

    public void setValue(float value) {
        Value = value;
    }

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    public Date getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(Date issueDate) {
        IssueDate = issueDate;
    }

    @XmlTransient
    public int getCourseId() {
        return CourseId;
    }

    public void setCourseId(int courseId) {
        CourseId = courseId;
    }

    @XmlTransient
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    @XmlTransient
    public int getStudentId() {
        return StudentId;
    }

    public void setStudentId(int studentId) {
        StudentId = studentId;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}

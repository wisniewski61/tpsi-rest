package com.example.rest.Models;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Queue;

/**
 * Created by user on 16.04.17.
 */
public class DataContext {
    private static Datastore datastore;
    private static int lastStudentId = 0;
    private static int lastCourseId = 0;
    private static int lastGradeId = 0;
    private static List<Student> students = new ArrayList<>();
    private static List<Course> courses = new ArrayList<>();

    public DataContext() {
        if (datastore == null)
            initDatastore();
        students = datastore.find(Student.class).asList();
        courses = datastore.find(Course.class).asList();
        if (students.isEmpty() && courses.isEmpty()) {
            initStudents();
            initCourses();
            initGrades();
            datastore.save(students);
        }//if
    }//DataContext()

    private void initDatastore() {
        Morphia morphia = new Morphia();
        morphia.mapPackage("com.example.rest.Models");
        MongoClient mongo = new MongoClient("localhost", 27017);
        //if (mongo.getDatabase("mydatastore") != null)
          //  mongo.getDatabase("mydatastore").drop();

        datastore = morphia.createDatastore(new MongoClient("localhost", 27017), "mydatastore");
        datastore.ensureIndexes();
    }

    private void initGrades() {
        try {
            addGrade(new Grade(3.0f, new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-21"), 1), 1);
            addGrade(new Grade(3.5f, new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-21"), 1), 2);
            addGrade(new Grade(2.0f, new SimpleDateFormat("yyyy-MM-dd").parse("2017-03-22"), 2), 1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void initStudents() {
        try {
            addStudent(new Student("Jan", "Kowalski", new SimpleDateFormat("yyyy-MM-dd").parse("1993-11-30")), true);
            addStudent(new Student("Marek", "Nowak", new SimpleDateFormat("yyyy-MM-dd").parse("1994-07-12")), true);
            addStudent(new Student("Marek", "Kowalski", new SimpleDateFormat("yyyy-MM-dd").parse("1994-09-12")), true);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        datastore.save(students);
    }

    private void initCourses() {
        addCourse(new Course("Programowanie", "prof. Wybitny"), true);
        addCourse(new Course("Bazy danych", "prof. Mądry"), true);

        datastore.save(courses);
    }//initCourses()

    public int addStudent(Student student, boolean init) {
        lastStudentId++;
        student.setIndex(lastStudentId);
        if (init)
            students.add(student);
        else
            datastore.save(student);
        return lastStudentId;
    }

    public Student getStudentByIndex(int index) {
        List<Student> allStudents = this.getStudents();
        for (Student student :
                allStudents) {
            if (student.getIndex() == index)
                return student;
        }//for

        return null;
    }//findStudentByIndex()

    public List<Student> getStudents() {
        students = datastore.find(Student.class).asList();
        return students;
    }

    public List<Student> getStudents(String firstName, String lastName, String birthdate, Boolean before) {
        Query<Student> query = datastore.createQuery(Student.class);
        if(firstName!=null)
            query.criteria("FirstName").containsIgnoreCase(firstName);
        if(lastName!=null)
            query.criteria("LastName").containsIgnoreCase(lastName);
        if(birthdate!=null) {
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(birthdate);
                if(before==null)
                    query.filter("Birthdate", date);
                else {
                    if(before.booleanValue())
                        query.field("Birthdate").lessThan(date);
                    else
                        query.field("Birthdate").greaterThan(date);
                }//else
            } catch (ParseException e) {
                e.printStackTrace();
            }//catch
        }//if
        students = query.asList();
        return students;
    }//getStudents()

    public boolean updateStudent(Student student) {
        Student updatedStudent = this.getStudentByIndex(student.getIndex());
        if (updatedStudent == null)
            return false;

        updatedStudent.setFirstName(student.getFirstName());
        updatedStudent.setLastName(student.getLastName());
        updatedStudent.setBirthdate(student.getBirthdate());
        datastore.save(updatedStudent);
        return true;
    }

    public boolean deleteStudent(Student student) {
        datastore.delete(student);
        return true;
    }

    public List<Course> getCourses() {
        courses = datastore.find(Course.class).asList();
        return courses;
    }

    public List<Course> getCourses(String lecturer) {
        if(lecturer==null)
            return getCourses();
        Query<Course> query = datastore.createQuery(Course.class);
        query.criteria("Lecturer").containsIgnoreCase(lecturer);
        courses = query.asList();
        return courses;
    }//getCourses()

    public Course getCourseById(int id) {
        List<Course> courseList = datastore.find(Course.class).asList();
        for (Course course :
                courseList) {
            if (course.getCourseId() == id) return course;
        }//for
        return null;
    }//getCourseById()

    public int addCourse(Course course, boolean init) {
        lastCourseId++;
        course.setCourseId(lastCourseId);
        if (init)
            courses.add(course);
        else
            datastore.save(course);
        return lastCourseId;
    }//addCourse()

    public boolean updateCourse(Course course) {
        Course updatedCourse = this.getCourseById(course.getCourseId());
        if (updatedCourse == null)
            return false;

        updatedCourse.setLecturer(course.getLecturer());
        updatedCourse.setName(course.getName());
        datastore.save(updatedCourse);
        return true;
    }//updateCourse()

    public boolean deleteCourse(Course course) {
        datastore.delete(course);
        getStudents();
        for (Student student :
                students) {
            for (Grade grade :
                    student.getGrades()) {
                if (grade.getCourseId() == course.getCourseId())
                    this.deleteGrade(grade);
            }//for
        }//for
        return true;
    }//deleteCourse()

    public List<Grade> getStudentGrades(int studentId) {
        for (Student s :
                students) {
            if (s.getIndex() == studentId) return s.getGrades();
        }//foreach

        return new ArrayList<>();
    }//getStudentGrades()

    public List<Grade> getStudentGrades(int studentId, String coursename) {
        //Query<Grade.class>
        //TODO: filtering student grades
        return null;
    }

    public Grade getGradeById(int id) {
        students = this.getStudents();
        for (Student student :
                students) {
            for (Grade grade :
                    student.getGrades()) {
                if (grade.getId() == id)
                    return grade;
            }//for
        }//for

        return null;
    }//getGradeById()

    private Student getGradeOwner(int gradeId) {
        getStudents();
        for (Student student :
                students) {
            for (Grade grade :
                    student.getGrades()) {
                if (grade.getId() == gradeId)
                    return student;
            }//for
        }//for

        return null;
    }

    public int addGrade(Grade grade, int studentId) {
        getStudents();
        lastGradeId++;
        grade.setId(lastGradeId);
        //grades.add(grade);
        for (Student student :
                students) {
            if (student.getIndex() == studentId) {
                grade.setStudentId(studentId);
                //grade.setStudent(student);
                student.getGrades().add(grade);
                datastore.save(student);
            }//if
        }//for
        return lastGradeId;
    }//addGrade()

    public boolean updateGrade(Grade grade) {
        Grade updatedGrade = null;
        Student updatedStudent = this.getGradeOwner(grade.getId());
        if(updatedStudent !=null) {
            for (Grade g :
                    updatedStudent.getGrades()) {
                if (g.getId() == grade.getId())
                    updatedGrade = g;
            }//for
        }//if
        if (updatedGrade == null)
            return false;

        updatedGrade.setCourseId(grade.getCourseId());
        updatedGrade.setStudentId(grade.getStudentId());
        updatedGrade.setIssueDate(grade.getIssueDate());
        updatedGrade.setValue(grade.getValue());
        updatedGrade.setCourse(getCourseById(grade.getCourse().getCourseId()));
        datastore.save(updatedStudent);
        return true;
    }//updateGrade

    public boolean deleteGrade(Grade grade) {
        getStudents();
        for (Student stu :
                students) {
            for (Grade g :
                    stu.getGrades()) {
                if(g.getId()==grade.getId()) {
                    stu.getGrades().remove(g);
                    datastore.save(stu);
                    return true;
                }//if
            }
        }//for
        return false;
    }
}

package com.example.rest.Models;

import com.example.rest.Controllers.StudentsController;
import com.example.rest.Models.Grade;

import com.example.rest.ObjectIdJaxbAdapter;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.bson.types.ObjectId;
import org.glassfish.jersey.linking.Binding;
import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;
import org.mongodb.morphia.annotations.*;

import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 15.04.17.
 */
@XmlRootElement
@Entity("students")
public class Student {
    @InjectLinks({
            @InjectLink(resource = StudentsController.class, rel = "parent"),
            //@InjectLink(resource = StudentsController.class, method = "getStudent", bindings = @Binding(name = "index", value = "${instance.getIndex()}"), rel = "self"),
            @InjectLink(value="students/{index}", rel = "self"),
            @InjectLink(value="students/{index}/grades", rel = "grades")
    })
    @XmlElement(name="link")
    @XmlElementWrapper(name = "links")
    @XmlJavaTypeAdapter(Link.JaxbAdapter.class)
    List<Link> links;

    @Id
    @XmlTransient
    private ObjectId id;

    @Indexed(name = "index", unique = true)
    private int Index;
    private String FirstName;
    private String LastName;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    private Date Birthdate;
    private ArrayList<Grade> grades;

    public Student() {
        FirstName = "";
        LastName = "";
        Birthdate = new Date();
        grades = new ArrayList<>();
    }

    public Student(String firstName, String lastName, Date birthdate) {
        FirstName = firstName;
        LastName = lastName;
        if(birthdate==null)
            birthdate = new Date();
        Birthdate = birthdate;
        grades = new ArrayList<>();
    }

    public void setIndex(int index) { Index = index; }

    public void setFirstName(String firstName) { FirstName = firstName; }

    public void setLastName(String lastName) { LastName = lastName; }

    public void setBirthdate(Date birthdate) {
        Birthdate = birthdate;
    }

    public int getIndex() { return Index; }

    public String getFirstName() { return FirstName; }

    public String getLastName() { return LastName; }

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    public Date getBirthdate() { return Birthdate; }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Grade> grades) {
        this.grades = grades;
    }

    @XmlTransient
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
